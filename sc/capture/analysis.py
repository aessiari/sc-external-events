import functools
import pickle
import uuid

from sc.db import models
from sc.capture import base, exceptions


class StepInfo:
    def __init__(self, step=None, index=None, data_in=None, data_out=None):
        self.step = step

        self.index = index
        self.data_in = data_in
        self.data_out = data_out
        self.errors = []


class DebugInfo:

    def __init__(self, caller=None):
        self.caller = caller
        self._items = {}

    def __len__(self):
        return len(self._items)

    def __iter__(self):
        return iter(self._items.values())

    def __getitem__(self, *args, **kwargs):
        return self._items.__getitem__(*args, **kwargs)

    def __setitem__(self, *args, **kwargs):
        self._items.__setitem__(*args, **kwargs)

    def add_step(self, key, **kwargs):
        step_info = StepInfo(**kwargs)
        self[key] = step_info

        return step_info

    @property
    def has_errors(self):
        return any(bool(step_info.errors) for step_info in self)

    def get_picklable_data(self):
        """
        Return a subset of this object's data in a format compatible with being serialized
        using the `pickle` module.
        """

        return {name: step.data_in for name, step in self._items.items()}


class LinearPipeline:
    """
    Convenience wrapper around a sequence of callables to represent a basic linear pipeline
    where the output of the nth step is used as the input of the (n + 1)th.
    """

    def __init__(self, steps=None, **kwargs):
        # TODO in general this is just for LoggingMixin
        super().__init__()

        self.steps = steps or self.default_steps
        self._params = kwargs

    @property
    def default_steps(self):
        return []

    @property
    def params(self):
        return self._params

    def clone(self, steps=None, **kwargs):
        factory = type(self)
        steps = list(steps or self.steps)
        params = dict(self.params, **kwargs)
        return factory(steps=steps, **params)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.steps[key]
        elif isinstance(key, slice):
            return self.clone(steps=self.steps[key])
        else:
            raise TypeError(f'Value "{key}" of type "{type(key)}" is not supported')

    def __len__(self):
        return len(self.steps)

    def __iter__(self):
        return iter(self.steps)

    def get_step_name(self, step):
        # TODO add alternatives/fallbacks for other types if necessary
        return step.__qualname__

    def run_debug(self, data):
        # TODO change this from a dict to something fancier
        debug_info = DebugInfo(caller=self)

        for step_idx, step in enumerate(self):
            step_name = self.get_step_name(step)
            step_info = debug_info.add_step(step_name, step=step, index=step_idx)
            step_info.data_in = data
            try:
                data = step(data)
            except Exception as cause_exc:
                step_info.errors.append(cause_exc)
                exc = exceptions.ProcessingError(f'An error occurred at step {step_idx} ({step_name})')
                exc.debug_info = debug_info
                raise exc from cause_exc
            else:
                step_info.data_out = data

        return data, debug_info

    def run(self, *args, **kwargs):
        res, debug_info = self.run_debug(*args, **kwargs)
        return res

    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)


class RawEventsAnalyzer(base.Ingester):
    """
    Generate events by fetching and processing raw events.
    """

    raw_event_model = models.RawEvent
    event_model = models.Event

    @property
    def raw_event_manager(self):
        return self.raw_event_model.incremental

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.processor = None

    def get_raw_events(self):
        # since we're using the DB here, we should also deal with
        # general DB access/connection errors
        try:
            to_process = self.raw_event_manager.since_last_processed()
        except self.raw_event_model.DoesNotExist as e:
            raise exceptions.NothingToProcess('No raw events found in database') from e

        return to_process

    def process(self, raw_events):

        try:
            raw_df = raw_events.to_dataframe()
            if raw_df.empty:
                raise exceptions.NothingToProcess
            self.log.info('%d raw event(s) to process', len(raw_df))
            processed = self.processor.run(raw_df)
            return processed
        except exceptions.ProcessingError:
            raise

    def run(self):
        processed_events = []
        try:
            raw_events = self.get_raw_events()
            processed_events = self.process(raw_events)
            n_saved = self.save(processed_events)
            self.register_as_processed(raw_events, errors=None)
        except exceptions.NothingToProcess:
            pass
        except exceptions.ProcessingError as e:
            self.log.error('Encountered error during processing')
            self.handle_processing_error(e, raw_events=raw_events)
        except exceptions.DatabaseSaveError as save_error:
            self.log.error('Could not save processed events!')
            self.log.exception(save_error)
        except Exception as e:
            self.log.error('An unexpected error occurred')
            self.log.exception(e)
        else:
            pass

        return processed_events

    def register_as_processed(self, raw_events, errors=None):
        msg = 'Registering raw events as processed'
        if errors:
            msg += ' with errors'

        self.log.debug(msg)

        def is_simple(x):
            return x is None or isinstance(x, (bool, str))

        if not is_simple(errors):
            errors = str(errors)

        self.raw_event_manager.register_processed(raw_events, errors=errors)

    def handle_processing_error(self, exc, raw_events=None):
        self.log.exception(exc)
        # NOTE in case of a processing error, we'd like to keep the events unprocessed in case
        # the processing will be successful after new raw events are added
        # (i.e. the error occurs because of incomplete input data)
        # however, if this is not the case and the input data is invalid,
        # this would cause the analysis to constantly fail
        # so we mark the events as processed, adding information that an error occurred
        if raw_events:
            self.register_as_processed(raw_events, errors=True)
        # save dump of data in debug info to help debug processing issues offline
        # TODO think if there are better alternatives to doing it here
        from sc.config import CONFIG_DIR

        dump_id = uuid.uuid4()
        dump_dir = CONFIG_DIR / str(dump_id)
        self.log.error('Saving debug info to: %s', dump_dir)
        self.create_dump_from_debug_info(exc.debug_info, dump_dir)

    def create_dump_from_debug_info(self, debug_info, dump_dir):

        self.log.info('Creating directory %s', dump_dir)
        dump_dir.mkdir()

        pickle_path = dump_dir / 'debug-info.pickle'
        self.log.info('Extracting serializable data from debug info')
        picklable_data = debug_info.get_picklable_data()
        self.log.info('Saving data to binary file %s', pickle_path)
        with pickle_path.open('wb') as f:
            pickle.dump(picklable_data, f)
