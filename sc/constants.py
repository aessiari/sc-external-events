"""
All different constants
"""

import os

class CapsuleObject(object):
    """
    Different object types in Science Capsule --
        DATA: data; refers to input/output data files
        EXE: executables; refers to scripts/apps/binaries that can be executed;
        NOTE: notes; scans of lab notebooks, or digital notes
    """
    ACTOR = 'ACTOR'
    DATA = 'DATA'
    EXE = 'EXE'
    NOTE = 'NOTE'
    TIMELINES= 'TIMELINES'


class CapsuleView(object):
    TIMELINE = 'timeline'
    PROVENANCE = 'provenance'
